import React from 'react';
import './contact-details.scss'
const ContactDetails = () => {
    return (
        <div className="contact-details bg-white flex-1 w-12">
            <div className="cd-header flex justify-between p-4">
                <div className="action-button">
                    <div className="pr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
                        </svg>
                    </div>
                    Contacts
                </div>
                <div className="action-button">
                    Actions
                    <div className="pl-1">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="#7c98b6" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                        </svg>
                    </div>
                </div>
            </div>
            <div className="contact-info flex p-4">

                <img className="rounded-full h-16 w-16"  src="https://i.pravatar.cc/300"/>
                <div className="contact-box pl-4">
                    <div className="contact-name font-light text-2xl">Maria Johnson</div>
                    <div className="contact-designation text-sm font-extralight">CEO</div>
                    <div className="contact-email flex items-center">
                        <div className="text-sm font-extralight flex">maria.johnson@hubspot.com</div>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 ml-1" viewBox="0 0 20 20" fill="#7c98b6">
                            <path d="M7 9a2 2 0 012-2h6a2 2 0 012 2v6a2 2 0 01-2 2H9a2 2 0 01-2-2V9z" />
                            <path d="M5 3a2 2 0 00-2 2v6a2 2 0 002 2V5h8a2 2 0 00-2-2H5z" />
                        </svg>
                    </div>
                </div>
            </div>
            <div className="contact-icons flex justify-around p-4 border-gray-300 border-b">
                <div className="contact-icon-container flex flex-col items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-blue-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                        </svg>
                    </div>
                    <div className="title">Note</div>
                </div>
                <div className="contact-icon-container flex flex-col  items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207" />
                        </svg>
                    </div>
                    <div className="title">Email</div>
                </div>
                <div className="contact-icon-container flex flex-col  items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                        </svg>
                    </div>
                    <div className="title">Call</div>
                </div>
                <div className="contact-icon-container flex flex-col  items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </div>
                    <div className="title">Log</div>
                </div>
                <div className="contact-icon-container flex flex-col  items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z" />
                        </svg>
                    </div>
                    <div className="title">Task</div>
                </div>
                <div className="contact-icon-container flex flex-col  items-center">
                    <div className="contact-icon bg-gray-400 rounded-full p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                    </div>
                    <div className="title">Meet</div>
                </div>
            </div>
        </div>
    );
};

export default ContactDetails;