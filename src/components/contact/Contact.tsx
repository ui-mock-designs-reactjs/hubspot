import React from 'react';
import ContactDetails from "./details/ContactDetails";

const Contact = () => {
    return (
        <div className="contact flex flex-1">
            <ContactDetails></ContactDetails>
            <div className="flex flex-1"></div>
            <div className="flex flex-1"></div>
            <div className="flex flex-1"></div>
        </div>
    );
};

export default Contact;