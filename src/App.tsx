import React from 'react';
import './App.scss';
import Header from "./components/header/header";
import Contact from "./components/contact/Contact";

function App() {
  return (
    <div className="App flex flex-col min-h-screen bg-gray-400">
      <Header></Header>
        <Contact></Contact>
    </div>
  );
}

export default App;
